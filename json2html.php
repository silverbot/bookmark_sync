#!/usr/bin/env php
<?php
$json = file_get_contents($argv[1]);
$obj = json_decode($json,true);
foreach ($obj as $key => $value) {
	echo '<html><body><dl>';
	if (is_array($value)){
		parse($value);
	}
	echo '</dl></body></html>';	
}
function parse($value)
{

	foreach($value as $key => $value){
		if (is_array($value)){
			if(isset($value['title']) == true){
				if(isset($value['uri']))
					echo '<dd><a href="'.$value['uri'].'">'.$value['title']."</a></dd>\n";
				else{
					if($value['title'] ==="")
						echo '<br><hr size="2" width="100%"><br>';
					else
						echo '<dt>['.$value['title']."]</dt>\n";
				}
			}
			parse($value);
		}
	}
}
?>
